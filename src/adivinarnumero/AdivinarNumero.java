/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adivinarnumero;

import java.util.Scanner;

/**
 *
 * @author Alberto Ortega <1 DAW>
 * @version 2.0
 * @since 15.02.20
 */
public class AdivinarNumero {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int aleatorio;
        int numUsuario;
        int intentos;
        boolean repetir; 
        String volverJugar;
        
        do {
            repetir = false;
            intentos = 0;
            aleatorio = (int) Math.floor(Math.random() * 1000);
            System.out.println("Tienes 7 intentos para adivinar el número");
            do {

                System.out.println("Introduce un número entre el 1 y el 1000");
                numUsuario = sc.nextInt();

                if (aleatorio < numUsuario) {
                    System.out.println("Más bajo");
                } else if (aleatorio > numUsuario) {
                    System.out.println("Más alto");
                }
                //sumamos los intentos.
                intentos++;
                if (intentos == 7) {
                    break;
                }
                //mientras sea diferente se repite el bucle, hasta que sean iguales y aciertes.
            } while (aleatorio != numUsuario);
            if (aleatorio == numUsuario) {
                System.out.println("Lo has logrado en " + intentos + " intentos");
            }
            //confirmar la decisión del usuario 
            System.out.println("¿Quieres volver a jugar?");
            //doble nextLine porque Java se come un Intro
            volverJugar = sc.nextLine();
            volverJugar = sc.nextLine();
            
            if (volverJugar.equalsIgnoreCase("si")) {
                repetir = true;
            }
        } while (repetir == true);
    }

}
